from util1 import create_pos_neg
from util2 import Perceptron, perceptron_train, split_labeled_data
from util3 import generate_labeled_training_test_data, data_label_views

import numpy as np
import matplotlib.pyplot as plt

from dataclasses import dataclass
from collections import defaultdict

@dataclass
class Centroid:
    pos_centroid: np.ndarray
    neg_centroid: np.ndarray
    
    @staticmethod
    def train(pos, neg):
        pos_centroid = np.mean(pos, axis=0)
        neg_centroid = np.mean(neg, axis=0)
        return Centroid(pos_centroid, neg_centroid)
    
    def classify(self, x):
        pc = self.pos_centroid
        nc = self.neg_centroid
        is_pos = 0 < (pc - nc) @ x + (np.sum(nc**2) - np.sum(pc**2)) /  2
        return 1 if is_pos else -1

@dataclass
class NearestNeighbour:
    pos: np.ndarray
    neg: np.ndarray
        
    @staticmethod
    def train(pos, neg):
        return NearestNeighbour(pos, neg)
        
    def classify(self, x):
        is_pos = np.min(np.linalg.norm(x - self.pos, axis=1)) < np.min(np.linalg.norm(x - self.neg, axis=1))
        return 1 if is_pos else -1
    
@dataclass
class PerceptronClassifier:
    perceptron: Perceptron
    
    @staticmethod
    def _convert_input_data(pos, neg):
        n_pos, dim_pos = pos.shape
        n_neg, dim_neg = neg.shape

        if dim_pos != dim_neg:
            raise Exception("Incompatible shapes of pos and neg")
        dim = dim_pos
        
        labeled_pos = np.ones((n_pos, dim+1))
        labeled_pos[:,:-1] = pos
        
        labeled_neg = np.ones((n_neg, dim+1)) * -1
        labeled_neg[:,:-1] = neg
        
        data_points = np.concatenate((labeled_neg, labeled_pos), axis=0)
        
        np.random.shuffle(data_points)

        return data_points[:,:-1], data_points[:, dim:]
    
    @staticmethod
    def train(pos, neg, **kwargs):
        train_data = PerceptronClassifier._convert_input_data(pos, neg)
        perc = perceptron_train(*train_data, **kwargs)
        return PerceptronClassifier(perc)
    
    def classify(self, x):
        is_pos = self.perceptron.weights_vec @ x + self.perceptron.bias_par > 0
        return 1 if is_pos else -1

@dataclass
class Evalutaion:
    error_count: float
    mean_err_percentage: float
    std_deviation: float
    class_size: int
        
    @staticmethod
    def count_errors(classifier, pos_test_data, neg_test_data):
        errors = 0
        
        for pos_x in pos_test_data:
            pos_class = classifier.classify(pos_x)
            if pos_class != 1:
                errors += 1
        for neg_x in neg_test_data:
            neg_class = classifier.classify(neg_x)
            if neg_class != -1:
                errors += 1
        
        return errors
    
    @staticmethod
    def _evaluate_single_iteration(classifier_class, class_size):
        train = create_pos_neg(class_size)
        test = create_pos_neg(class_size)
        classifier = classifier_class.train(*train)
        return Evalutaion.count_errors(classifier, *test)
    
    @staticmethod
    def evaluate_classifier(classifier_class, class_size, iterations=10):
        results = [Evalutaion._evaluate_single_iteration(classifier_class, class_size) for i in range(iterations)]
        error_count = np.sum(results)
        mean_err_percentage = np.mean(results) / class_size
        std_deviation = np.std(results)
        
        return Evalutaion(error_count, mean_err_percentage, std_deviation, class_size)
    

def evaluate_classifiers(classifier_classes, iterations = 10):
    results = defaultdict(list)
    for n in range(10, 201, 10):
        for classifier_cls in classifier_classes:
            results[classifier_cls.__name__].append(Evalutaion.evaluate_classifier(classifier_cls, n, iterations=iterations))
    return results

def plot_eval_results(centroid_results, nn_results):
    plt.figure(figsize=(6,6))
    centroid_mean_err_data = np.array([(ev.class_size, ev.mean_err_percentage) for ev in centroid_results])
    nn_mean_err_data = np.array([(ev.class_size, ev.mean_err_percentage) for ev in nn_results])
    plt.title('Mean error percentage')
    plt.plot(*centroid_mean_err_data.transpose(),'r', *nn_mean_err_data.transpose(), 'b')
    plt.legend(("Centroid", "Nearest Neighbour"))
    plt.show()
    
    plt.figure(figsize=(6,6))
    centroid_std_deviation_data = np.array([(ev.class_size, ev.std_deviation) for ev in centroid_results])
    nn_std_deviation_data = np.array([(ev.class_size, ev.std_deviation) for ev in nn_results])
    plt.title('Std deviation')
    plt.plot(*centroid_std_deviation_data.transpose(),'r', *nn_std_deviation_data.transpose(), 'b')
    plt.legend(("Centroid", "Nearest Neighbour"))


def evaluate_classifiers_on_tis_data(classifier_classes, iterations=10):
    results = defaultdict(list)
    for classifier_cls in classifier_classes:
        for i in range(iterations):
            training, test = generate_labeled_training_test_data()
            pos_train, neg_train = split_labeled_data(*data_label_views(training))
            pos_test, neg_test = split_labeled_data(*data_label_views(test))
            
            classifier = classifier_cls.train(pos_train, neg_train)
            results[classifier_cls.__name__].append(
                Evalutaion.count_errors(classifier, pos_test, neg_test))
    return results

def plot_results(results):
        mean_err = {classifier: np.mean(error_counts) for classifier, error_counts in results.items()}
        
        plt.figure(figsize=(6,6))
        plt.title('Mean errors of classifier on TIS Data')
        plt.ylabel('Mean error')
        plt.bar([1,2,3], mean_err.values(), tick_label=[key for key in mean_err.keys()])