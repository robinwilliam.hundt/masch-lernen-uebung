from util1 import *
from dataclasses import dataclass


def create_perceptron_input(
    n, 
    dim=2, 
    neg_shift=np.array([0.9, 0.9,]), 
    shuffle=True, 
    guaranteed_seperable=False):
    
    pos, neg = create_pos_neg(n, dim=dim, neg_shift=neg_shift)
    if guaranteed_seperable:
        while do_conv_hulls_overlap(pos, neg):
            pos, neg = create_pos_neg(n, dim=dim, neg_shift=neg_shift)

    labeled_pos = np.ones((n, dim+1))
    labeled_pos[:,:-1] = pos
    labeled_neg = np.ones((n, dim+1)) * -1
    labeled_neg[:,:-1] = neg
    
    data_points = np.concatenate((labeled_neg, labeled_pos), axis=0)
    
    if shuffle:
        np.random.shuffle(data_points)
    
    return data_points[:,:-1], data_points[:, dim:]

@dataclass
class Perceptron:
    terminated: bool
    weights_vec: np.ndarray
    bias_par: float
    n_updates_train: int
    iterations: int
    R: float

def perceptron_train(x_train_mat, y_train_vec, eta_par=0.1, max_iterations=500):
    n = len(y_train_vec)
    dim = x_train_mat.shape[1]
    
    w = np.zeros(dim)
    b = 0.0
    
    R = max(np.linalg.norm(x) for x in x_train_mat)
    K = 0
    
    terminated = True
    
    for i in range(0, max_iterations):
        K_old = K
        for x,y in zip(x_train_mat, y_train_vec):
            if y * (w @ x + b) <= 0:
                w = w + eta_par * y * x
                b = b + eta_par * y * R**2
                K += 1
                
        if K == K_old:
            break
    else:
        terminated = False
            
    return Perceptron(terminated=terminated, weights_vec=w, bias_par=b, n_updates_train=K, iterations=i, R=R)

def split_labeled_data(x_mat, y_vec):
    pos = []
    neg = []
    for x, y in zip(x_mat, y_vec):
        if y == -1:
            neg.append(x)
        else:
            pos.append(x)
    return np.array(pos), np.array(neg)

def calc_perceptron_seperation_line(perceptron):
    w1, w2 = perceptron.weights_vec
    b = perceptron.bias_par
    
    y_for_x = lambda x: -(w1*x+b)/w2
    
    return np.array([(x, y_for_x(x)) for x in range(3)])

def plot_perceptron(perceptron, x_mat, y_vec):
    pos, neg = split_labeled_data(x_mat, y_vec)
    plot_pos_neg(pos, neg)
    line = calc_perceptron_seperation_line(perceptron)
    plt.plot(*line.transpose(), '-')


def distance_point_to_line(line_point_1, line_point_2, point):
    p1x, p1y = line_point_1
    p2x, p2y = line_point_2
    x, y = point
    
    nominator = abs((p2y - p1y)*x - (p2x - p1x)*y + p2x*p1y - p2y*p1x)
    denominator = math.sqrt((p2y - p1y)**2 + (p2x - p1x)**2)
    return nominator / denominator

def min_dist_to_line(points, line_point_1, line_point_2):
    return min(abs(distance_point_to_line(line_point_1, line_point_2, p))
               for p in points)

def upper_error_bound(perceptron, points):
    R = perceptron.R
    p1, p2, _ = calc_perceptron_seperation_line(perceptron)
    gamma = min_dist_to_line(points, p1, p2)
    
    return (2*R/gamma)**2


def perceptron_test(x_test_mat, y_test_vec, weights_vec, bias_par):
    errors = 0
    for x, y in zip(x_test_mat, y_test_vec):
        if y * (weights_vec @ x + bias_par) <= 0:
            errors += 1
    return errors

@dataclass
class PerceptronEval:
    error_count: int
    mean_err: float
    std_deviaton: float
    min_err: int
    max_err: int
    mean_err_probability: float
        
    @staticmethod
    def from_results(results, n_test=None):
        error_count = sum(results)
        mean_err = np.mean(results)
        std_deviaton = np.std(results)
        min_error = min(results)
        max_err = max(results)
        mean_err_probability = None
        if n_test is not None:
            mean_err_probability = mean_err / n_test
        return PerceptronEval(error_count, mean_err, std_deviaton, min_error, max_err, mean_err_probability)


def test_perceptron_performance(n_train=50, n_test=100, n_datasets=10):
    results = []
    for i in range(n_datasets):
        train_data = create_perceptron_input(n=n_train, guaranteed_seperable=True)
        test_data = create_perceptron_input(n=n_test)
        perceptron = perceptron_train(*train_data)

        results.append(perceptron_test(*test_data, perceptron.weights_vec, perceptron.bias_par))
    
    return PerceptronEval.from_results(results)    

