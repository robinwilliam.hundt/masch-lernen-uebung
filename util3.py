import numpy as np
from util2 import perceptron_train, perceptron_test, PerceptronEval

ALPHABET = ('A', 'C', 'G', 'T')


def append_label_column(mat, label=1.):
    n, dim = mat.shape
    labeled = np.ones((n, dim+1)) * label
    labeled[:,:-1] = mat
    return labeled

def read_pos_neg(pos_file_name='pos.seq', neg_file_name='neg.seq'):
    with open('neg.seq')as n, open('pos.seq') as p:
        neg = [line.strip() for line in n.readlines()]
        pos = [line.strip() for line in p.readlines()]
    return pos, neg

    
def _one_hot_helper(seq, el):
    return np.array(tuple(seq)) == el

def one_hot_seq(seq, alphabet=ALPHABET):
    return np.hstack([_one_hot_helper(seq, ch) for ch in alphabet])

def one_hot_seqs(sequences, alphabet=ALPHABET):
    return np.vstack([one_hot_seq(seq, alphabet=alphabet) for seq in sequences])

def generate_labeled_training_test_data(**kwargs):
    pos, neg = read_pos_neg(**kwargs)
    one_hot_pos = one_hot_seqs(pos).astype(float)
    one_hot_neg = one_hot_seqs(neg).astype(float)
        
    one_hot_pos = append_label_column(one_hot_pos, label=1.)
    one_hot_neg = append_label_column(one_hot_neg, label=-1.)
    
    data = np.vstack((one_hot_pos, one_hot_neg))
    
    permuted = np.random.permutation(data)
    
    middle_row = permuted.shape[0] // 2
    
    training = permuted[:middle_row, :]
    test = permuted[middle_row:, :]
    
    return training, test

def data_label_views(labeled_data):
    return labeled_data[:, :-1], labeled_data[:, -1]

def test_perceptron_for_tis_performance(runs, eta_par=0.1):
    results = []
    for i in range(runs):
        training, test = generate_labeled_training_test_data()
        perceptron = perceptron_train(*data_label_views(training), eta_par=eta_par)
        results.append(perceptron_test(
            *data_label_views(test),
            perceptron.weights_vec,
            perceptron.bias_par))
    
    return PerceptronEval.from_results(results, test.shape[0])    