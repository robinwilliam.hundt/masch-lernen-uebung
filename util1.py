import math

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull, convex_hull_plot_2d, Delaunay


def create_pos_neg(n, neg_shift=[0.9, 0.9], dim=2):
    pos = np.random.rand(n, dim)
    neg = np.random.rand(n, dim) + neg_shift
    return pos, neg

def plot_pos_neg(pos_points, neg_points, pos_marker='b.', neg_marker='r.', figsize=(8,8), show_convex_hull=False, title=""):
    plt.figure(figsize=figsize)
    plt.title(title)

    if show_convex_hull:
        pos_hull = ConvexHull(pos_points)
        neg_hull = ConvexHull(neg_points)
        for pos_simplex in pos_hull.simplices:
            plt.plot(pos_points[pos_simplex, 0], pos_points[pos_simplex, 1], 'k-')
        for neg_simplex in neg_hull.simplices:
            plt.plot(neg_points[neg_simplex, 0], neg_points[neg_simplex, 1], 'k-')
        
    plt.plot(*pos_points.transpose(), pos_marker, *neg_points.transpose(), neg_marker)
    
def is_left(a, b, c):
    b_min_a = b - a
    c_min_a = c - a
    return b_min_a[0] * c_min_a[1] - b_min_a[1] * c_min_a[0] > 0

def is_right(*args):
    return not is_left(*args)

def extract_hull_points(hull):
    points = set()
    for p1, p2 in hull.simplices:
        points.add(p1)
        points.add(p2)
    return np.array([hull.points[p] for p in points])
    
def sort_hull_points(hull_points):
    centroid = hull_points.mean(axis=0)
    hull_points = list(hull_points)
    hull_points.sort(key=lambda p: math.atan2(p[1]-centroid[1],p[0]-centroid[0]))
    return hull_points

    
def is_point_in_hull(sorted_hull_points, point):
    sp = sorted_hull_points
    for p_from, p_to in zip(sp, sp[1:] + sp[0:1]):
        if is_right(p_from, p_to, point):
            return False
    return True

def do_conv_hulls_overlap(points1, points2):
    hull = ConvexHull(points1)
    hull_points = extract_hull_points(hull)
    sorted_hull_points = sort_hull_points(hull_points)
    for p in points2:
        if is_point_in_hull(sorted_hull_points, p):
            return True
    return False
